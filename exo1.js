const readline = require('readline');

var input = [];
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.prompt();

rl.on('line', function (cmd) {
	input.push(cmd);
});

rl.on('close', function (cmd) {

	let max = parseInt(input[0]);

	for (let i = 1; i <= max; i++){

		let out = i;

		if (i % 3 == 0) out = "Fizz";
		if (i % 5 == 0) out = "Buzz";
		if (i % 3 == 0 && i % 5 == 0) out = "FizzBuzz";

		console.log(out);
	}

	process.exit(0);
});