const readline = require('readline');

var input = [];
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.prompt();

rl.on('line', function (cmd) {
	input.push(cmd);
});

rl.on('close', function (cmd) {

	let nb_web = parseInt(input.shift());
	let nb_hyper = parseInt(input.shift());
	let hypers = [], tmp;
	let resu = 1;

	for (let i = 0; i < input.length; i++){
		hypers[parseInt(input[i].split(' ')[1])] = 1;
	}

	hypers = hypers.filter(Number);
	if (hypers.length < nb_web) resu = 0;

	console.log(resu);

	process.exit(0);
});