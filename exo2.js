const readline = require('readline');

var input = [];
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.prompt();

rl.on('line', function (cmd) {
	input.push(cmd);
});

rl.on('close', function (cmd) {

	let nb = parseInt(input.shift());
	let tmp, houses, money;

	for (let i = 0; i < input.length; i++){

		tmp = input[i].split(' ');
		houses = parseInt(tmp[0]);
		money = parseInt(tmp[1]);

		if (houses > 1){
			houses--;
			money = (Math.floor(houses / 2, 1) + 1) * money;
		}

		console.log(money);
	}


	process.exit(0);
});