const readline = require('readline');

var input = [];
var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.prompt();

rl.on('line', function (cmd) {
	input.push(cmd);
});

rl.on('close', function (cmd) {

	let size = parseInt(input.shift());
	let list = [];
	let min, max, tmp;

	for (let i = 0; i < input.length; i++){
		list.push(parseInt(input[i]));
	}

	list.sort(function(a, b) {
		return a - b;
	});

	min = list[list.length-1];
	max = min - list[0];

	for (let i = 1; i < list.length; i++){
		tmp = list[i] - list[i - 1];

		if (tmp < min) min = tmp;
	}

	console.log(min, max);

	process.exit(0);
});